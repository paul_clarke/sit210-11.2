import pyb
from utime import time
import uasyncio as asyncio
from machine import UART
from micropython_ra8875.widgets.sliders import HorizSlider
from micropython_ra8875.driver.tft_local import setup
from micropython_ra8875.fonts import numbers105, freesans20
from micropython_ra8875.widgets.label import Label
from micropython_ra8875.py.colors import CYAN
from micropython_ra8875.py.ugui import Screen


class CountDown:
    # Class to hold coundown values - if second screen implemented this would be accessed by both.
    timer_value = str(1)
    timer_end = None
    race_started = False


class Buzzer:
    # Simple class to hold the buzzer function
    pin = pyb.Pin("Y2")
    tim = pyb.Timer(8, freq=3000)
    ch = tim.channel(2, pyb.Timer.PWM, pin=pin)

    def buzz(self):
        self.ch.pulse_width_percent(30)
        pyb.delay(550)
        self.ch.pulse_width_percent(0)


class Screen_1(Screen):
    def __init__(self):
        super().__init__()
        # imports classes from outside the Screen class
        self.count_down_value = CountDown()
        self.buzzer = Buzzer()

        # Creates slider for the tilt value
        self.tilt_slider = HorizSlider(
            (50, 400), fgcolor=CYAN, width=700, divisions=2, border=2)

        # Creates labels
        self.timer_label = Label(
            (270, 20), font=numbers105, height=20, width=50, border=0, fontcolor=CYAN)
        self.count_down_value.timer_value = '5:00'  # Default start value
        self.timer_label.value(self.count_down_value.timer_value)
        self.speed_display = Label(
            (250, 230), font=numbers105, height=20, width=50, border=0, fontcolor=CYAN)
        self.speed_display.value('0kn')  # Default value before GPS initialised

        # Starts async functions
        self.reg_task(self.change_var())
        self.reg_task(self.timer_button())
        self.reg_task(self.speed_reading())
        self.reg_task(self.tilt_update())

    async def tilt_update(self):
        # Update slider bar as a result of the accellerometer changing
        accel = pyb.Accel()
        while True:
            await asyncio.sleep_ms(500)
            angle = accel.x()
            if angle > 0 and angle < 20 and angle > -20:
                self.tilt_slider.value(0.5-(angle*0.025))
            else:
                self.tilt_slider.value(((-angle)*0.025)+0.5)

    async def speed_reading(self):
        # Initialise GPS
        self.gps = UART(1, 9600)
        self.gps.init(9600, bits=8, parity=None, stop=1)
        self.reading = asyncio.StreamReader(self.gps)
        self.buf = ''
        # Checks for empty sentences and only acknowledges GPRMC sentences, when there is a valid sentence the speed is updated
        while True:
            res = await self.reading.readline()
            if res is not None:
                self.buf += res.decode()
                if '\r\n' in self.buf:
                    line = self.buf.slit['\r\n']
                    if line[0] == '$GPRMC':
                        if line[7] is not None:
                            self.speed_display.value(
                                str(round(line[7], 0)) + 'kn')

    async def change_var(self):
        self.beep_values = [50, 40, 30, 20, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
        self.prev = 0
        while True:
            await asyncio.sleep_ms(200)
            # Waits for button to be pressed which sets timer_end variable
            # Once timer_end is set it is compared to the current time
            # When race_started is True, the timer will count up
            if self.count_down_value.timer_end is not None:
                if self.count_down_value.race_started == False:
                    seconds = self.count_down_value.timer_end - time()
                    if seconds < 0:
                        self.count_down_value.race_started = True
                    else:
                        seconds_output = '{:0>{w}}'.format(
                            str(seconds % 60), w=2)
                        if seconds > 60:
                            minute_output = str(seconds // 60)
                            self.count_down_value.timer_value = str(
                                minute_output + ':' + seconds_output)
                        else:
                            self.count_down_value.timer_value = '0:' + \
                                str(seconds_output)
                        self.timer_label.value(
                            self.count_down_value.timer_value)
                    if seconds != self.prev:
                        # Buzz every minute while it is counting down, every 10 seconds during the last minute and ever second for the last 10 seconds
                        if seconds % 60 == 0 or seconds in self.beep_values:
                            self.buzzer.buzz()
                    self.prev = seconds
                else:
                    seconds = time() - self.count_down_value.timer_end
                    seconds_output = '{:0>{w}}'.format(str(seconds % 60), w=2)
                    if seconds < 60:
                        self.count_down_value.timer_value = '0:' + \
                            str(seconds_output)
                    else:
                        minute_output = '{:0>{w}}'.format(
                            str(seconds // 60), w=2)
                        self.count_down_value.timer_value = str(
                            minute_output + ':' + seconds_output)
                    self.timer_label.value(self.count_down_value.timer_value)

    async def timer_button(self):
        # Instead of using the set library for the capacitive button this was implemented as lighter weight
        # It compares the current state to the previous state, if it is changed to 0 (pressed) it changes the variable
        sensor = pyb.Pin('X2', pyb.Pin.IN, pull=pyb.Pin.PULL_UP)
        prev_val = 1
        self.timer_running = False
        while True:
            curr_val = sensor.value()
            if curr_val != prev_val:
                if self.timer_running == False:
                    self.count_down_value.timer_end = time() + 5 * 60
                    self.timer_label.value(self.count_down_value.timer_value)
                    self.timer_running = True
                elif self.count_down_value.race_started == False:
                    seconds = self.count_down_value.timer_end - time()
                    self.count_down_value.timer_end = int(
                        60 * round(float(seconds)/60)) + time()
            prev_val = curr_val
            await asyncio.sleep_ms(50)


setup()
Screen.change(Screen_1)
